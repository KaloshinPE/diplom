# -*- coding: utf-8 -*-
from __future__ import absolute_import

import sys
import os
import re
import numpy as np
import pandas as pd
from itertools import chain
import pymorphy2
from functools import reduce
from gensim.models import Word2Vec


def load_data(folders):
    """
    Грузим данные из папок по списку, аггрегируем
    :param folders: список папок
    :return: два массива из словарей с ключами 'lecture', 'question', 'answers', 'true_answers'.
                        Каждый словарь - объект выборки.
    """
    data = []
    raw_data = []
    for elem in folders:
        print elem
        d1, d2 = load_from_folder(elem)
        data += d1
        raw_data += d2
    return data, raw_data


def print_sentence(sentence):
    """
    Выводим список юникодных слов в человеческом виде
    :param sentence: список из слов
    """
    for i, elem in enumerate(sentence):
        sys.stdout.write('"' + elem + '"')
        if i < len(sentence) - 1:
            sys.stdout.write(',')
        else:
            sys.stdout.write('\n')


def load_from_folder(folder):
    """
    :param folder: папка, из которой читаем данные
    :return: два массива из словарей с ключами 'lecture', 'question', 'answers', 'true_answers'.
                        Каждый словарь - объект выборки.
    """

    def string_to_list(string, sep=', '):
        return string.strip('()').split(sep)

    morph = pymorphy2.MorphAnalyzer()

    lectures_files = [x for x in os.listdir(folder) if x.isalnum()]
    lectures = {}
    raw_lectures = {}
    for elem in lectures_files:
        lectures[int(elem)] = []
        raw_lectures[int(elem)] = []
        with open('/'.join([folder, elem]), 'r') as f:
            for sentence in re.split('[.!?]', f.read()):
                tokenized_sentence = tokenize(sentence, morph)
                if len(tokenized_sentence) > 1:
                    raw_lectures[int(elem)].append(sentence)
                    lectures[int(elem)].append(tokenized_sentence)

    questions = pd.read_csv(folder + '/' + 'answers.csv').dropna()

    data = {}
    raw_data = {}

    for key in lectures.keys():
        lecture_questions = questions[questions.lecture == key][['question', 'answer', 'true_answer']].as_matrix()
        if len(lecture_questions) == 0:
            break
        data[key] = []
        raw_data[key] = []
        for question, variants, answer in lecture_questions:
            new_line = {'question': tokenize(question, morph),
                        'answers': map(lambda x: tokenize(x, morph), string_to_list(variants, '|')),
                        'true_answers': map(lambda x: int(x.strip(', ')) - 1, string_to_list(answer))}
            # критерий того, что правильные ответы могут быть выучены в рамках модели (в нем есть токенизируемые слова)
            valid_answers_criterion = True
            for k in new_line['true_answers']:
                if len(new_line['answers'][k]) == 0:
                    valid_answers_criterion = False
                    break
            if len(new_line['answers']) < 8 and valid_answers_criterion:
                data[key].append(new_line)
                new_raw_line = {'question': question,
                                'answers': string_to_list(variants, '|'),
                                'true_answers': map(lambda x: int(x.strip(', ')) - 1, string_to_list(answer))}
                raw_data[key].append(new_raw_line)

    def merge_with_lectures(data, lectures):
        ret = []
        for key in data:
            ret.append({
                'lecture': lectures[key],
                'test_corpus': data[key]
            })
        return ret

    return merge_with_lectures(data, lectures), merge_with_lectures(raw_data, raw_lectures)


def tokenize(sent, morph=None):
    """
    :param sent: предложение в виде строки
    :return: список из отдельных нормализованных слов
    """
    if morph is None:
        morph = pymorphy2.MorphAnalyzer()

    ret = [re.sub(u"[^а-яА-Я0-9\s]", "", x.strip()) for x in re.split('((?u)\W+)?', sent.decode('utf-8')) \
            if re.sub(u"[^а-яА-Я0-9\s]", "", x.strip())]

    def morph_wrap(x):
        variants = morph.parse(x)
        if len(variants) > 0:
            return variants[0].normal_form
        else:
            return x

    return list(map(morph_wrap, ret))


def index(data):

    def extract_words(object):
        lecture_words = sum(object['lecture'], [])
        other_words = sum([sum(elem['answers'], []) + elem['question'] for elem in object['test_corpus']], [])
        return set(lecture_words + other_words)

    words_sets = [extract_words(object) for object in data]
    vocab = sorted(reduce(lambda x, y: x | y, words_sets))
    word_to_index = dict((c, i + 1) for i, c in enumerate(vocab))  # ноль зарезервирован под пустые слова
    return word_to_index


def index_to_representation(index_to_word, embedding_size=300):
    """
    Создаем словарик, сопоставляющий индекс слова его векторному представлению. Если word2vec содержит нужный вектор,
        то добавляем его, если нет - вставляем рандомный.
    :param index_to_word: словарь соответствия index: word
    :param embedding_size: размерность word2vec векторов
    :return: словарь соответствия index: vector, маску для фильтрации ненайденных слов, ненайденные слова
    """
    ret = {}
    index_to_mask = {}
    not_found = []
    md = Word2Vec.load('../w2w/ru.bin')
    w2w_counter = 0
    for index, word in index_to_word.iteritems():
        if word in md.wv:
            index_to_mask[index] = 0
            ret[index] = md.wv[word]
            w2w_counter += 1
        else:
            index_to_mask[index] = 1
            not_found.append(word)
            ret[index] = 5*np.random.rand(embedding_size)
    print('w2w representation: нашлось {} слов из {}'.format(w2w_counter, len(index_to_word)))
    ret[0] = np.zeros(embedding_size)
    return ret, index_to_mask, not_found


def nn_memory_params(data, memory_size=None):
    lec_sentence_size = max(map(len, chain.from_iterable(elem['lecture'] for elem in data)))

    def zero_max(sequence):
        if len(sequence) > 0:
            return max(sequence)
        return 0

    def max_len_of_iterable(iterable):
        return zero_max(map(len, iterable))

    question_size = max((max_len_of_iterable((x['question'] for x in elem['test_corpus'])) for elem in data))
    answers_size = max((zero_max([max_len_of_iterable(x['answers']) for x in elem['test_corpus']]) for elem in data))
    answers_number = max((max_len_of_iterable((x['true_answers'] for x in elem['test_corpus'])) for elem in data))

    max_lecture_size = max(map(len, (elem['lecture'] for elem in data)))

    if memory_size is not None:
        memory_size = min(memory_size, max_lecture_size)
        if max_lecture_size > memory_size:
            print('='*30, '\n', 'WARNING: memory overflow')
            print('memory size:\t', memory_size)
            print('lecture size:\t', max_lecture_size)
            print('='*30)
    else:
        memory_size = max_lecture_size

    sentence_size = max([lec_sentence_size, answers_size, question_size])
    return memory_size, sentence_size, answers_number


def vectorize_data(data, word_to_index, max_memory_size, sentence_size, answers_number):
    """
    Векторизуем данные для подачи в сетку
    :param data: токенизированные данные для подачи в сетку в формате, возвращаемом load_data
    :param word_to_index: словарь соответствия слова индексу
    :param max_memory_size: число ячеек памяти (максимальная длина истории)
    :param sentence_size: максимальная длина предложения
    :param answers_number: число вариантов ответа
    :return: четыре массива с токенизированными данными: истории, вопросы, варианты ответов, правильные
    """
    ret = []

    for elem in data:
        # lecture, question, answers, true_answers = elem['lecture'], elem['question'], \
        #                                            elem['answers'], elem['true_answers']
        lecture, test_corpus = elem['lecture'], elem['test_corpus']

        ss = []
        for i, sentence in enumerate(lecture, 1):
            ls = max(0, sentence_size - len(sentence))
            ss.append([word_to_index[w] for w in sentence] + [0] * ls)

        # take only the most recent sentences that fit in memory
        ss = ss[::-1][:max_memory_size][::-1]

        ret.append([np.array(ss), []])
        for elem2 in test_corpus:
            question, answers, true_answers = elem2['question'], elem2['answers'], elem2['true_answers']

            lq = max(0, sentence_size - len(question))
            q = [word_to_index[w] for w in question] + [0] * lq

            aa = []
            for i, sentence in enumerate(answers, 1):
                ls = max(0, sentence_size - len(sentence))
                aa.append([word_to_index[w] for w in sentence] + [0] * ls)

            # pad to answers_number
            lm = max(0, answers_number - len(aa))
            for _ in range(lm):
                aa.append([0] * sentence_size)

            ta = np.zeros(answers_number)
            ta[map(lambda x: x, true_answers)] = 1
            ret[-1][1].append(map(np.array, [q, aa, ta]))

    return ret


def vec_to_words(sentence, idx_word):
    ret = ''
    for elem in sentence:
        ret += idx_word[elem] + ' '
    return ret
