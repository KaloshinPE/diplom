# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division

import tensorflow as tf
import numpy as np


def add_gradient_noise(t, stddev=1e-3, name=None):
    """
    Adds gradient noise as described in http://arxiv.org/abs/1511.06807 [2].
    The input Tensor `t` should be a gradient.
    The output will be `t` + gaussian noise.
    0.001 was said to be a good fixed value for memory networks [2].
    """
    with tf.name_scope(name, "add_gradient_noise", [t, stddev]) as name:
        t = tf.convert_to_tensor(t, name="t")
        gn = tf.random_normal(tf.shape(t), stddev=stddev)
        return tf.add(t, gn, name=name)


def variable_initialisation_corrections(variable, shape):
    return (variable - 0.5) * 2 # * np.sqrt(6 / sum(shape))


class MemN2N(object):
    """End-To-End Memory Network."""
    def embedding_init(self, embedding_matrix):
        W = tf.Variable(tf.constant(0.0, shape=embedding_matrix.shape),
                        trainable=True, name="word_embedding")

        embedding_placeholder = tf.placeholder(tf.float32, embedding_matrix.shape, name='word_embedding_placeholder')
        embedding_init = W.assign(embedding_placeholder)

        self._sess.run(embedding_init, feed_dict={embedding_placeholder: embedding_matrix})

        return W

    def __init__(
                self,
                batch_size,
                sentence_size,
                memory_size,
                answers_number,
                embedding_matrix,
                embedding_mask,
                max_grad_norm=40.0,
                initializer=tf.random_normal_initializer(stddev=0.1),
                gru_hidden_layer_size=200,
                sentences_embedding_size=200,
                session=tf.Session(),
                name='MemN2N'):
        self._sess = session
        self._batch_size = batch_size
        self._vocab_size, self._embedding_size = embedding_matrix.shape
        self._sentence_size = sentence_size
        self._memory_size = memory_size
        self._answers_number = answers_number
        self._encoding = tf.Variable(
                                        tf.constant(embedding_matrix, shape=embedding_matrix.shape, dtype=tf.float32),
                                        trainable=True,
                                        name="word_embedding")
        self._embedding_mask = tf.Variable(
                                            tf.constant(embedding_mask, shape=embedding_mask.shape, dtype=tf.float32),
                                            trainable=False,
                                            name="word_embedding_mask")
        self._gru_hidden_layer_size = gru_hidden_layer_size
        self._sentences_embedding_size = sentences_embedding_size

        self._queries_form_matrix = tf.Variable(
                                                variable_initialisation_corrections(
                                                    tf.random_uniform(
                                                        (2 * self._sentences_embedding_size,
                                                         self._sentences_embedding_size)),
                                                    (2 * self._sentences_embedding_size,
                                                     self._sentences_embedding_size)),
                                                name="question_form_matrix")

        self._answer_form_matrix = tf.Variable(
                                                variable_initialisation_corrections(
                                                                    tf.random_uniform(
                                                                        (2*self._sentences_embedding_size,
                                                                        self._sentences_embedding_size)),
                                                                    (2*self._sentences_embedding_size,
                                                                    self._sentences_embedding_size)),
                                                name="answer_form_matrix")

        self._sentence_embedding_matrix = tf.Variable(
                                                variable_initialisation_corrections(
                                                                    tf.random_uniform(
                                                                        (self._gru_hidden_layer_size,
                                                                        self._sentences_embedding_size)),
                                                                    (self._gru_hidden_layer_size,
                                                                    self._sentences_embedding_size)),
                                                name="sentence_embedding_matrix")
        self._max_grad_norm = max_grad_norm
        self._init = initializer
        self._name = name

        self._build_inputs()

        self._opt = tf.train.AdamOptimizer(learning_rate=self._lr)

        # cross entropy
        logits, attention_probs = self._inference(self._story, self._queries, self._answers)
        cross_entropy = tf.nn.sigmoid_cross_entropy_with_logits(logits=logits,
                                                                labels=tf.cast(self._true_answers, tf.float32),
                                                                name="cross_entropy")
        # введем штраф на локализацию ответа в одном предложении
        attention_regularization = tf.reduce_mean((attention_probs)**2, axis=1, name='attention_regularisation')

        self.cross_entropy_loss = tf.reduce_sum(cross_entropy)
        self.regularization_loss = 500 * tf.reduce_sum(attention_regularization)
        loss_op = tf.add(self.cross_entropy_loss, self.regularization_loss, name="loss")

        # gradient pipeline
        grads_and_vars = self._opt.compute_gradients(loss_op)
        grads_and_vars = [(tf.clip_by_norm(g, self._max_grad_norm), v)
                          for g, v in grads_and_vars if g is not None]
        grads_and_vars = [(add_gradient_noise(g, name='noisy_gradient'), v) for g, v in grads_and_vars]

        # наложим маску на градиент матрицы эмбеддинга, чтобы не обучать найденные вектора w2w

        def mask_embedding_grad(g, v):
            if v.name.endswith('word_embedding:0'):
                return tf.multiply(g, tf.expand_dims(self._embedding_mask, axis=1), name='masking_embedding_matrix'), v
            return g, v

        grads_and_vars = [mask_embedding_grad(g, v) for g, v in grads_and_vars]

        train_op = self._opt.apply_gradients(grads_and_vars, name="train_op")

        # predict ops
        predict_op = tf.nn.sigmoid(logits, name='predictions')

        self.loss_op = loss_op
        self.predict_op = predict_op
        self.attention_probs_op = attention_probs
        self.train_op = train_op

        init_op = tf.global_variables_initializer()
        self._sess.run(init_op)

    def _build_inputs(self):
        self._story = tf.placeholder(tf.int32, [None, self._sentence_size], name="stories")
        self._queries = tf.placeholder(tf.int32, [None, self._sentence_size], name="queries")
        self._answers = tf.placeholder(tf.int32, [None, self._answers_number, self._sentence_size], name="answers")
        self._true_answers = tf.placeholder(tf.int32, [None, self._answers_number], name="answers")
        self._lr = tf.placeholder(tf.float32, [], name="learning_rate")

    def _sentences_embedding(self, sentences, name=None):

        lens = tf.reduce_sum(tf.sign(sentences), 1)
        sentences = tf.nn.embedding_lookup(self._encoding, sentences)

        try:
            _, state = tf.nn.dynamic_rnn(
                tf.contrib.rnn.GRUCell(self._gru_hidden_layer_size),
                sentences,
                dtype=tf.float32,
                sequence_length=lens,
            )
        except ValueError:
            _, state = tf.nn.dynamic_rnn(
                tf.contrib.rnn.GRUCell(self._gru_hidden_layer_size, reuse=True),
                sentences,
                dtype=tf.float32,
                sequence_length=lens,
            )

        if name is not None:
            return tf.matmul(state, self._sentence_embedding_matrix, name=name)
        else:
            return tf.matmul(state, self._sentence_embedding_matrix)

    def _data_embedding(self, story, queries, answers):

        with tf.variable_scope('embeddings'):
            answers = tf.reshape(answers, (-1, self._sentence_size))
            story = self._sentences_embedding(story, 'embedded_story')
            queries = self._sentences_embedding(queries, 'embedded_queries')
            answers = self._sentences_embedding(answers)

            answers = tf.reshape(answers,
                                 (-1, self._answers_number, self._sentences_embedding_size),
                                 name='embedded_answers')

            return story, queries, answers

    def _inference(self, story, queries, answers):
        with tf.variable_scope(self._name):
            # маска для ответов, учет разного числа ответов в примерах
            answers_number_mask = tf.cast(tf.sign(tf.reduce_sum(answers, -1)),
                                      tf.float32,
                                      name='answers_number_mask')

            story, queries, answers = self._data_embedding(story, queries, answers)

            # модифицируем вектор вопроса с учетом вариантов ответов
            answers_attention = tf.nn.softmax(tf.reduce_sum(tf.expand_dims(queries, 1) * answers, -1), -1)
            queries = tf.matmul(
                                tf.concat(
                                    [tf.reduce_sum(answers*tf.expand_dims(answers_attention, -1), 1), queries],
                                    -1),
                                self._queries_form_matrix,
                                name='transformed_queries')

            # считаем softmax
            cos_diff = tf.reduce_sum(tf.expand_dims(queries, 1) * tf.expand_dims(story, 0), 2, name='cos_diff')

            log_softmax_denominator = tf.reduce_logsumexp(cos_diff,
                                                          axis=1,
                                                          keep_dims=True,
                                                          name='log_softmax_denominator')
            attention_probs = tf.exp(tf.subtract(cos_diff, log_softmax_denominator), name='attention_probs')

            # аггрегируем информацию из текста согласно значениям attention
            information = tf.matmul(
                                    tf.concat(
                                            [tf.reduce_sum(tf.expand_dims(story, 0) *
                                                                            tf.expand_dims(attention_probs, 2),
                                                           axis=1,
                                                           name='information'),
                                             queries],
                                            axis=1),
                                    self._answer_form_matrix,
                                    name='information_aggregation')

            # считаем степени похожести вариантов ответа на правильные (косинусная мера разности с вектором информации)
            # вычитаем из отсутствующих ответов большое число, чтобы сигмоида обращала их в ноль
            logits = tf.add(tf.reduce_sum(tf.expand_dims(information, 1) * answers, 2) / 10**3 * answers_number_mask,
                            tf.subtract(answers_number_mask, 1) * 10 ** 18,
                            name='logits')

            return logits, attention_probs

    def batch_fit(self, story, queries, answers, true_answers, learning_rate, summary_list=None):

        feed_dict = {self._story: story, self._queries: queries, self._answers: answers,
                     self._true_answers: true_answers, self._lr: learning_rate}
        summary_list = [self.cross_entropy_loss, self.regularization_loss]
        if summary_list is None:
            summary_list = []

        cross_entropy_loss, regularisation_loss, loss, _ = self._sess.run(summary_list + [self.loss_op, self.train_op], feed_dict=feed_dict)
        return cross_entropy_loss, regularisation_loss, loss

    def predict(self, story, queries, answers):
        feed_dict = {self._story: story, self._queries: queries, self._answers: answers}
        return self._sess.run(self.predict_op, feed_dict=feed_dict)

    def attention(self, story, queries, answers):
        feed_dict = {self._story: story, self._queries: queries, self._answers: answers}
        return self._sess.run(self.attention_probs_op, feed_dict=feed_dict)
