# -*- coding: utf-8 -*-

import tensorflow as tf
import numpy as np
from tensorflow.python import debug as tf_debug
import argparse
import pickle
import os
import time

from memn2n import MemN2N
import data_utils as du

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--debug', action='store_true')
    return parser.parse_args()


class Runner:
    def __init__(
                self,
                learning_rate=0.1,
                anneal_rate=25,
                anneal_stop_epoch=150,
                max_grad_norm=40.0,
                evaluation_interval=10,
                batch_size=32,
                epochs=300,
                memory_size=300,
                sentence_embedding_size=200,
                gru_hidden_layer_size=200,
                debug=False,
                saved_model_path=None):
        """

        :param session: tf.session
        :param learning_rate: Learning rate for SGD.
        :param anneal_rate: Number of epochs between halving the learnign rate.
        :param anneal_stop_epoch: Epoch number to end annealed lr schedule.
        :param max_grad_norm: Clip gradients to this norm.
        :param evaluation_interval: Evaluate and print results every x epochs
        :param batch_size: Batch size for training.
        :param hops: Number of hops in the Memory Network.
        :param epochs: Number of epochs to train for.
        :param memory_size: Maximum size of memory.
        :param word_embedding_size: Word embedding size.
        :param random_state: Random state.
        """
        self.learning_rate = learning_rate
        self.anneal_rate = anneal_rate
        self.anneal_stop_epoch = anneal_stop_epoch
        self.max_grad_norm = max_grad_norm
        self.evaluation_interval = evaluation_interval
        self.batch_size = batch_size
        self.epochs = epochs
        self.memory_size = memory_size
        self.word_embedding_size = 300 # size of pre-trained w2w
        self.sentence_embedding_size = sentence_embedding_size
        self.gru_hidden_layer_size = gru_hidden_layer_size

        self.datalist = None
        self.raw_data = None
        self.data = None
        self.word_index = None
        self.index_to_word = None
        self.vectorized_data = None

        self.memory_size = None
        self.sentence_size = None
        self.answer_number = None
        self.vocab_size = None

        self.embedding_matrix = None
        self.embedding_mask = None
        self.model = None
        self.model_is_load = False
        self.saved_model_path = saved_model_path
        self.graph = tf.Graph()
        self.sess = tf.Session(graph=self.graph)

        if debug:
            self.sess = tf_debug.LocalCLIDebugWrapperSession(self.sess)
            self.sess.add_tensor_filter('has_inf_or_nan', tf_debug.has_inf_or_nan)
            # self.sess = tf_debug.TensorBoardDebugWrapperSession(self.sess, "pavel-N551JM:6054")


    @staticmethod
    def batches(data, n=1):
        for lecture, test_corpus in data:
            tests_number = len(test_corpus)
            for ndx in range(0, tests_number, n):
                yield [np.array(lecture)] + list(map(np.array, zip(*test_corpus[ndx:min(ndx + n, tests_number)])))

    @staticmethod
    def accuracy(true_answers, preds):
        preds = np.around(preds)
        return np.sum(np.sum(true_answers == preds, axis=1) == true_answers.shape[1])/float(len(true_answers))

    @staticmethod
    def test_example(data, n=5):
        return Runner.batches(data, n).next()

    def read_data(self, preprocessed_data=None):
        if preprocessed_data is None:
            self.data, self.raw_data = du.load_data(self.datalist)
        else:
            self.data, self.raw_data = preprocessed_data
        if not self.model_is_load:
            self.word_index = du.index(self.data)
            self.memory_size, self.sentence_size, self.answers_number = du.nn_memory_params(self.data, self.memory_size)

        self.index_to_word = {v: k for k, v in self.word_index.iteritems()}
        self.index_to_word[0] = '#buf#'
        self.vocab_size = len(self.word_index) + 1

        print '=' * 30, '\n', 'network parameters:'
        print 'memory_size:\t', self.memory_size
        print 'sentence_size:\t', self.sentence_size
        print 'answers_number:\t', self.answers_number
        print 'vocab_size:\t', self.vocab_size
        print '=' * 30

        if not self.model_is_load:
            index_to_vector, index_to_mask, _ = du.index_to_representation(self.index_to_word, self.word_embedding_size)
            self.embedding_matrix = np.array([v for v in index_to_vector.itervalues()])
            self.embedding_mask = np.array([v for v in index_to_mask.itervalues()])

        self.vectorized_data = du.vectorize_data(self.data, self.word_index, self.memory_size,
                                                 self.sentence_size, self.answers_number)

    def train_data_accuracy(self):
        train_preds = []
        for s, q, a, _ in Runner.batches(self.vectorized_data, self.batch_size):
            pred = self.model.predict(s, q, a)
            train_preds += list(pred)

        true_answers = []
        for _, test_corpus in self.vectorized_data:
            for test_question in test_corpus:
                true_answers.append(test_question[2])

        train_accuracy = Runner.accuracy(np.array(true_answers), train_preds)
        return train_accuracy

    def fit(self, datalist=('../data/algorithms_algocombi', ), preprocessed_data=None):
        self.datalist = datalist
        with self.graph.as_default():
            if not self.model_is_load:
                if self.saved_model_path is not None:
                    self.load_model(self.saved_model_path)

                if not self.model_is_load:
                    self.read_data(preprocessed_data)
                    with self.graph.as_default():
                        self.model = MemN2N(batch_size=self.batch_size,
                                            sentence_size=self.sentence_size,
                                            memory_size=self.memory_size,
                                            answers_number=self.answers_number,
                                            embedding_matrix=self.embedding_matrix,
                                            embedding_mask=self.embedding_mask,
                                            sentences_embedding_size=self.sentence_embedding_size,
                                            gru_hidden_layer_size=self.gru_hidden_layer_size,
                                            session=self.sess,
                                            max_grad_norm=self.max_grad_norm)
                self.model_is_load = True

            for t in range(1, self.epochs+1):
                begin = time.time()
                # Stepped learning rate
                if t - 1 <= self.anneal_stop_epoch:
                    anneal = 2.0 ** ((t - 1) // self.anneal_rate)
                else:
                    anneal = 2.0 ** (self.anneal_stop_epoch // self.anneal_rate)
                lr = self.learning_rate / anneal

                total_cost = 0.0
                total_count = 0
                total_entropy = 0
                total_regularization = 0
                for s, q, a, ta in Runner.batches(self.vectorized_data, self.batch_size):
                    entropy, regularization, cost = self.model.batch_fit(s, q, a, ta, lr)
                    if entropy > 10**10:
                        print entropy, regularization
                        print s, '\n-----------------\n', q, '\n-----------------\n', a, '\n-----------------\n', ta
                    total_cost += cost
                    total_entropy += entropy
                    total_regularization += regularization
                    total_count += len(q)
                end =time.time()

                total_cost /= float(total_count)
                total_regularization /= float(total_count)
                total_entropy /= float(total_count)

                if t % self.evaluation_interval == 0:

                    s, q, a, ta = Runner.test_example(self.vectorized_data, n=3)
                    test_preds = self.model.predict(s, q, a)

                    train_accuracy = self.train_data_accuracy()

                    timedelta = int(end-begin)
                    print '-----------------------'
                    print 'Epoch\t\t', t, 'took {:02d}:{:02d}:{:02d}'.format(timedelta / 3600,
                                                                             timedelta % 3600 / 60,
                                                                             timedelta % 60)
                    print 'Total Cost:\t\t', total_cost, ' entropy: {}, regularization: {}'\
                                                                    .format(total_entropy, total_regularization)
                    print 'Training Accuracy:\t', train_accuracy
                    print 'Predictions:\n', test_preds
                    print 'True_answers:\n', ta
                    print '-----------------------'

    def prediction(self, story_number, query_number):
        story = self.vectorized_data[story_number][0]
        queries, answers, _ = zip(*self.vectorized_data[story_number][1])
        query, answer = [queries[query_number]], [answers[query_number]]
        return self.model.predict(story, query, answer)

    def attention(self, story_number, query_number):
        story = self.vectorized_data[story_number][0]
        queries, answers, _ = zip(*self.vectorized_data[story_number][1])
        query, answer = [queries[query_number]], [answers[query_number]]
        return self.model.attention(story, query, answer)

    def joint_attention(self, story_number):
        story = self.vectorized_data[story_number][0]
        queries, answers, _ = zip(*self.vectorized_data[story_number][1])
        attentions = (self.model.attention(story, queries, answers))
        return 1 - np.prod(1 - attentions, axis=0)

    def save_model(self, path):
        """
        :param path: path to directory with model. there should be two files: with weights and with hyperparameters
        """
        os.mkdir(path)
        with self.graph.as_default():
            tf.train.Saver().save(self.sess, path + '/weights.save')
        hyperparameters = {
            'batch_size': self.batch_size,
            'sentence_size': self.sentence_size,
            'memory_size': self.memory_size,
            'answers_number': self.answers_number,
            'embedding_matrix': self.embedding_matrix,
            'embedding_mask': self.embedding_mask,
            'max_grad_norm': self.max_grad_norm,
            'word_index': self.word_index,
            'datalist': self.datalist,
            'sentence_embedding_size': self.sentence_embedding_size,
            'gru_hidden_layer_size': self.gru_hidden_layer_size,
            'learning_rate': self.learning_rate
        }
        with open(path + '/hyper.save', 'w') as f:
            pickle.dump(hyperparameters, f)

    def load_model(self, path, preprocessed_data=None):
        with open(path + '/hyper.save', 'r') as f:
            hyperparameters = pickle.load(f)

        self.batch_size = hyperparameters['batch_size']
        self.sentence_size = hyperparameters['sentence_size']
        self.memory_size = hyperparameters['memory_size']
        self.answers_number = hyperparameters['answers_number']
        self.embedding_matrix = hyperparameters['embedding_matrix']
        self.embedding_mask = hyperparameters['embedding_mask']
        self.max_grad_norm = hyperparameters['max_grad_norm']
        self.word_index = hyperparameters['word_index']
        self.datalist = hyperparameters['datalist']
        self.sentence_embedding_size = hyperparameters['sentence_embedding_size']
        self.gru_hidden_layer_size = hyperparameters['gru_hidden_layer_size']
        self.learning_rate = hyperparameters['learning_rate']

        with self.graph.as_default():
            self.model = MemN2N(batch_size=self.batch_size,
                                sentence_size=self.sentence_size,
                                memory_size=self.memory_size,
                                answers_number=self.answers_number,
                                embedding_matrix=self.embedding_matrix,
                                embedding_mask=self.embedding_mask,
                                sentences_embedding_size=self.sentence_embedding_size,
                                gru_hidden_layer_size=self.gru_hidden_layer_size,
                                session=self.sess,
                                max_grad_norm=self.max_grad_norm)

            tf.train.Saver().restore(self.sess, path + '/weights.save')
        self.model_is_load = True

        self.read_data(preprocessed_data)


    def __del__(self):
        self.sess.close()


if __name__ == '__main__':
    args = parse_args()
    runner = Runner(debug=args.debug, evaluation_interval=1, batch_size=16, epochs=500)

    with open('/home/pavel/memn2n_data_save', 'r') as f:
        data, raw_data = pickle.load(f)

    runner.fit(preprocessed_data=[data, raw_data])
